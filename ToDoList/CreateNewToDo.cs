using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Security;
using Newtonsoft.Json;

namespace ToDoList
{
    [Activity(Label = "CreateNewToDo")]
    public class CreateNewToDo : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.CreateLayout);

            EditText myEditTextForName = FindViewById<EditText>(Resource.Id.editTextForName);
            EditText myEditTextForDescription = FindViewById<EditText>(Resource.Id.editTextForDescription);

            Button myButtonForAdd = FindViewById<Button>(Resource.Id.buttonForAddNewToDo);
            myButtonForAdd.Enabled = true;
            myButtonForAdd.Click += (sender, args) =>
            {
                if (myEditTextForName.Text != string.Empty)
                { 
                    //MainActivity.mToDoList.Add(myEditTextForName.Text, myEditTextForDescription.Text);
                    //MainActivity.mAdapter.NotifyItemInserted(MainActivity.mToDoList.NumToDo);
                    var toDo = new ToDo();
                    toDo.Name = myEditTextForName.Text;
                    toDo.Discription = myEditTextForDescription.Text;
                    toDo.Deleting = false;
                    ToDoList.GetInstance().ToDos.Add(toDo);
                    
                    ToDoList.GetInstance().Save();


                    Toast.MakeText(this, "Element is added", ToastLength.Short).Show();
                    base.OnBackPressed();
                }
                else
                {
                    var dialog = new AlertDialog.Builder(this);
                    dialog.SetMessage("Please, enter ToDo name");
                    dialog.SetNegativeButton("Okay", delegate { });
                    dialog.SetNeutralButton("No", delegate
                    {
                        base.OnBackPressed();
                    });
                    dialog.Show();
                }
            };

            

        }
    }
}
