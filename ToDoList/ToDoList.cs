using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace ToDoList
{

    public class ToDo
    {
        public string mName;

        public string mDiscription;

        public bool mDeleting;

        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        public string Discription
        {
            get { return mDiscription;}
            set { mDiscription = value; }
        }

        public bool Deleting
        {
            get { return mDeleting; }
            set { mDeleting = value; }
        }
    }

    public class ToDoList : IDisposable
    {
        public List<ToDo> ToDos;

        private static ToDoList instance;

        private const string filename = "SavedList.txt";

        private ToDoList()
        {
            try
            {
                ToDos = JsonConvert.DeserializeObject<List<ToDo>>(LoadText(filename));
            }
            catch (Exception)
            {
                ToDos = new List<ToDo>();               
            }
        }

        public static ToDoList GetInstance()
        {
            return instance ?? (instance = new ToDoList());
        }

        private void SaveText(string filename, string text)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = System.IO.Path.Combine(documentsPath, filename);
            System.IO.File.WriteAllText(filePath, text);
            
        }
        private string LoadText(string filename)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = System.IO.Path.Combine(documentsPath, filename);
            return System.IO.File.ReadAllText(filePath);
        }

   /*     public void Add(string name, string description)
        {
            ToDo newToDo = new ToDo
            {
                Name = name,
                Discription = description,
                Deleting = false
            };
            ToDos.Add(newToDo);
        }*/

        public int NumToDo
        {
            get { return ToDos.Count; }
        }

        public ToDo this[int i]
        {
            get { return ToDos[i]; }
        }

        public void Dispose()
        {
            instance = null;
        }

        public void Save()
        {
            SaveText(filename, JsonConvert.SerializeObject(ToDos));
        }
    }
}