using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace ToDoList
{
    [Activity(Label = "DeletedToDo")]
    public class DeletedToDo : Activity
    {
        private RecyclerView mRecyclerView;

        private RecyclerView.LayoutManager mLayoutManager;
        
        public ToDoListDeletedAdapter mAdapterForDeleted;


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.DeletedLayout);

            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);

            mLayoutManager = new LinearLayoutManager(this);

            mRecyclerView.SetLayoutManager(mLayoutManager);

            mAdapterForDeleted = new ToDoListDeletedAdapter(ToDoList.GetInstance()); 

            mRecyclerView.SetAdapter(mAdapterForDeleted);

        }
    }

    public class ToDoViewHolderDeleted : RecyclerView.ViewHolder
    {
        public event EventHandler<int> ItemClick;
        public TextView Name { get; private set; }

        public TextView Description { get; private set; }

        public ToDoViewHolderDeleted (View itemView, Action<int> listener)
            : base(itemView)
        {
            Name = itemView.FindViewById<TextView>(Resource.Id.textViewName);
            Description = itemView.FindViewById<TextView>(Resource.Id.textViewDiscription);

            itemView.Click += (sender, e) => listener(base.Position);
        }

    }

    public class ToDoListDeletedAdapter : RecyclerView.Adapter
    {
        public event EventHandler<int> ItemClick;

        public ToDoList mToDoList;

        public ToDoListDeletedAdapter(ToDoList toDoList)
        {
            mToDoList = toDoList;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.DeletedListView, parent, false);
            ToDoViewHolderDeleted vh = new ToDoViewHolderDeleted(itemView, OnClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var items = mToDoList.ToDos.Where(x => x.Deleting).ToList();

            ToDoViewHolderDeleted vh = holder as ToDoViewHolderDeleted;

            vh.Name.Text = items[position].Name;
            if (items[position].Discription != string.Empty)
                vh.Description.Text = items[position].Discription;
            else
            {
                 vh.Description.Visibility = ViewStates.Gone;
            }
        }

        public override int ItemCount
        {
            get
            {
                return mToDoList.ToDos.Count(x => x.Deleting);
            }
        }

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }
    }
}