﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V7.Widget;
using Newtonsoft.Json;

namespace ToDoList
{
    [Activity(Label = "ToDoList", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private RecyclerView mRecyclerView;

        private Button buttonToNewActivity;

        private Button buttonForDeletedList;

        private RecyclerView.LayoutManager mLayoutManager;

        public ToDoListAdapter mAdapter;

        public ToDoList mToDoList = ToDoList.GetInstance();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            buttonToNewActivity = FindViewById<Button>(Resource.Id.buttonToNewActivity);
            buttonForDeletedList = FindViewById<Button>(Resource.Id.buttonToDeleted);

            mLayoutManager = new LinearLayoutManager(this);

            mRecyclerView.SetLayoutManager(mLayoutManager);

            if (mAdapter == null)
            {
                mAdapter = new ToDoListAdapter(mToDoList);
                mAdapter.DeleteItemClick += (sender, i) =>
                {
                    var dialog = new AlertDialog.Builder(this);
                    dialog.SetMessage("Want you  delete this ToDo?");
                    dialog.SetNeutralButton("No", delegate { });
                    dialog.SetNegativeButton("Yes", delegate
                    {
                        OnItemClick(sender, i);
                    });
                    dialog.Show();
                };
            }
 
            mRecyclerView.SetAdapter(mAdapter);
            buttonToNewActivity.Enabled = true;
            buttonToNewActivity.Click += (sender, args) =>
            {
                var intent = new Intent(this, typeof(CreateNewToDo));
                StartActivity(intent);
            };

            buttonForDeletedList.Enabled = true;
            buttonForDeletedList.Click += (sender, args) =>
            {
                var intent = new Intent(this, typeof(DeletedToDo));
                StartActivity(intent);
            };


        }

        protected override void OnResume()
        {
            base.OnResume();
            mAdapter.NotifyDataSetChanged();
        }

        void OnItemClick(object sender, int position)
        {
            var items = mToDoList.ToDos.Where(x => x.Deleting == false).ToList();
            items[position].Deleting = true;
            ToDoList.GetInstance().Save();
            mAdapter.NotifyDataSetChanged();
        }
    }

    public class ToDoViewHolder : RecyclerView.ViewHolder
    {
        public TextView Name { get; private set; }

        public TextView Description { get; private set; }

        public ToDoViewHolder(View itemView, Action<int> listener, Action<int> deleting)
            : base(itemView)
        {
            Name = itemView.FindViewById<TextView>(Resource.Id.textViewName);           
            Description = itemView.FindViewById<TextView>(Resource.Id.textViewDiscription);
            var deleteButton = itemView.FindViewById<Button>(Resource.Id.ButtonForDelete);

            itemView.Click += (sender, e) => listener(base.Position);
            deleteButton.Click += (sender, args) => deleting(base.Position);
        }

    }

    public class ToDoListAdapter : RecyclerView.Adapter
    {
        public event EventHandler<int> ItemClick;
        public event EventHandler<int> DeleteItemClick;

        public ToDoList mToDoList;

        public ToDoListAdapter(ToDoList toDoList)
        {
            mToDoList = toDoList;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ListView, parent, false);
            ToDoViewHolder vh = new ToDoViewHolder(itemView, OnClick, OnDelete);

            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var items = mToDoList.ToDos.Where(x => x.Deleting == false).ToList();

            ToDoViewHolder vh = holder as ToDoViewHolder;

            //var deleteButton = vh.ItemView.FindViewById<Button>(Resource.Id.ButtonForDelete);
/*            deleteButton.Click += (sender, args) =>
            {
                items[position].Deleting = true;
                NotifyDataSetChanged();
            };*/

            vh.Name.Text = items[position].Name;
            if (items[position].Discription != string.Empty)
                vh.Description.Text = items[position].Discription;
            else
            {
                vh.Description.Visibility = ViewStates.Gone;
            } 
        }


        public override int ItemCount
        {
            get
            {
                return mToDoList.ToDos.Count(x => !x.Deleting);
            }
        }

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }

        void OnDelete(int position)
        {
            if (DeleteItemClick != null)
                DeleteItemClick(this, position);
        }
    }

}

